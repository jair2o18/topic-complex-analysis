<!-- wp:paragraph -->
<p>Disable Guterburg from Wordpress</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>1)Put /10147?set-editor=classic and delete block-editor</p>
<!-- /wp:paragraph -->

<p> </p>
<div class="_1WODZhR-x-fbMu3MOL9cH1">Manually Searching OpenDirectories on Google</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">For videos/movies/tvshows :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">intext:"Search Term" intitle:"index.of" +(wmv|mpg|avi|mp4|mkv|mov) -inurl:(jsp|pl|php|html|aspx|htm|cf|shtml)
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Images :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">intext:"Search Term" intitle:"index.of./" (bmp|gif|jpg|png|psd|tif|tiff) -inurl:(jsp|pl|php|html|aspx|htm|cf|shtml)
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Music :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">intext:"Search Term" intitle:"index.of./" (ac3|flac|m4a|mp3|ogg|wav|wma) -inurl:(jsp|pl|php|html|aspx|htm|cf|shtml)
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Books :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">intitle:"Search Term" (pdf|epub|mob) "name or title" -inurl:(jsp|pl|php|html|aspx|htm|cf|shtml)
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">You can also find google drive shared files similarly.<br /><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.google.com/search?q=site%3Adrive.google.com+%2B%22drive%2Ffolders%22" target="_blank" rel="noopener noreferrer">Shared folders</a><br /><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.google.com/search?q=site%3Adrive.google.com" target="_blank" rel="noopener noreferrer">Shared everything</a><br />Works with other domains too.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Some info about google search operators can be found <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://web.archive.org/web/20180729112702/https://moz.com/learn/seo/search-operators" target="_blank" rel="noopener noreferrer">here</a></p>
<div class="_1WODZhR-x-fbMu3MOL9cH1">Search with the help of other tools</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://palined.com/search/" target="_blank" rel="noopener noreferrer">Palined</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://cgs.the-eye.eu/" target="_blank" rel="noopener noreferrer">The Eye</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://scrped.com/" target="_blank" rel="noopener noreferrer">SCRPED</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://lendx.org/" target="_blank" rel="noopener noreferrer">LENDX</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://www.filechef.com/" target="_blank" rel="noopener noreferrer">FileChef</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://ewasion.github.io/opendirectory-finder/" target="_blank" rel="noopener noreferrer">ewasion</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://lumpysoft.com/" target="_blank" rel="noopener noreferrer">lumpySoft</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.eyeofjustice.com/od/" target="_blank" rel="noopener noreferrer">EoJ OD.getter</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://opendirsearch.abifog.com/" target="_blank" rel="noopener noreferrer">OD Search Tool</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://doyou.needmorehdd.space/" target="_blank" rel="noopener noreferrer">NMHDDS</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://mattpalm.com/search/" target="_blank" rel="noopener noreferrer">Mattpalm</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://www.jimmyr.com/mp3_search.php" target="_blank" rel="noopener noreferrer">Jimmyr</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://filepursuit.com/" target="_blank" rel="noopener noreferrer">FilePursuit</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://od-db.the-eye.eu/" target="_blank" rel="noopener noreferrer">OD-Database</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://musgle.com/" target="_blank" rel="noopener noreferrer">Musgle</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://www.mmnt.net/" target="_blank" rel="noopener noreferrer">Mamont's open FTP Index</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.searchftps.net/" target="_blank" rel="noopener noreferrer">NAPALM FTP Indexer</a></p>
<div class="_1WODZhR-x-fbMu3MOL9cH1">Java Scriptlets</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Save the following code as a bookmark, then you can open the bookmark to run the desired action.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Download all files with a specific extension :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">javascript:!function(){var%20t=prompt("Enter%20filetype%20to%20download%20(format:%20.mp3)");if(null!==t)for(var%20e=document.querySelectorAll('[href$="'+t+'"]'),o=0;o<e.length;o++)e[o].setAttribute("download",""),e[o].click();else%20alert("No%20format")}();
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Resize "Filename" column in OD to make entire filename visible :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">javascript:!function(){function e(e){var o,n,r=e.href;e.textContent=(n=(o=r).split("/").filter(Boolean).reverse()[0],console.log(n),o.lastIndexOf("/")==o.lenght-1&&(n+="/"),n=n.indexOf(" ")>=0?decodeURI(n):decodeURIComponent(n))}anchors=document.body.querySelectorAll("a"),anchors=Array.from(anchors).slice(1),anchors.map(e)}();
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Display pictures as thumbnails :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">javascript:(function(){function%20I(u){var%20t=u.split('.'),e=t[t.length-1].toLowerCase();return%20{gif:1,jpg:1,jpeg:1,png:1,mng:1}[e]}function%20hE(s){return%20s.replace(/&/g,'&amp;').replace(/>/g,'&gt;').replace(/</g,'&lt;').replace(/"/g,'&quot;');}var%20q,h,i,z=open().document;z.write('<p>Images%20linked%20to%20by%20'+hE(location.href)+':</p><hr>');for(i=0;q=document.links[i];++i){h=q.href;if(h&&I(h))z.write('<p>'+q.innerHTML+'%20('+hE(h)+')<br><img%20src="'+hE(h)+'">');}z.close();})()
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Display pictures as thumbnail gallery :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">javascript:var%20sHTML=%22<html><head><title>gallery</title><body><center><table%20border=0>%22;var%20y=0;for(x=0;x<document.links.length;x++){a=document.links[x].href;%20if%20(a.match(/jpe|jpeg|jpg|bmp|tiff|tif|bmp|gif|png/i)){sHTML+='<td%20style=%22border-style:solid;border-width:1px%22><a%20target=%22_new%22%20href=%22'+a+'%22><img%20border=%220%22%20width=%22100%22%20src=%22'+a+'%22></a></td>';%20if%20(!((x+1)%5))%20sHTML+=%22</tr><tr>%22}};this.innerHTML=sHTML+%22</table></center></body></html>%22;
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">The-Eye Image Viewer :</p>
<pre class="_3GnarIQX9tD_qsgXkfSDz1"><code class="_34q3PgLsx9zIU5BiSOjFoM">javascript:void(window.open('https://fusker.the-eye.eu/url.php?url='+encodeURIComponent(document.URL).replace(/\./g,'%25252E')));
</code></pre>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Another <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://web.archive.org/web/20180803113222/http://fuseki.net/home/linked-images-bookmarklet.html" target="_blank" rel="noopener noreferrer">Linked Images Bookmarklet</a>.<br /><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.squarefree.com/bookmarklets/" target="_blank" rel="noopener noreferrer">More bookmarklets</a>.</p>
<div class="_1WODZhR-x-fbMu3MOL9cH1">Softwares</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://www.gnu.org/software/wget/" target="_blank" rel="noopener noreferrer">wget project</a><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://www.gnu.org/software/wget/" target="_blank" rel="noopener noreferrer"> page</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://eternallybored.org/misc/wget/" target="_blank" rel="noopener noreferrer">wget for Windows</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://chris.partridge.tech/data/wget-noobs.pdf" target="_blank" rel="noopener noreferrer">wget A Noobs Guide (PDF)</a> by <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/u/regravity/" target="_blank" rel="noopener noreferrer">u/regravity</a>.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://thomashunter.name/blog/install-wget-on-os-x-lion/" target="_blank" rel="noopener noreferrer">How to install wget on OSX</a> by <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/u/nucleocide/" target="_blank" rel="noopener noreferrer">u/nucleocide</a>.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">There are other softwares that provide wget with a GUI like <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://projects-old.gnome.org/gwget/" target="_blank" rel="noopener noreferrer">Gwget</a> and <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://winwget.sourceforge.net/" target="_blank" rel="noopener noreferrer">WinWGet</a> though I've never used them and hence can't comment on their reliability.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/rg3/youtube-dl" target="_blank" rel="noopener noreferrer">youtube-dl</a> (Python) downloads videos from various sites. Just like wget you can find GUI frontend for this.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/RipMeApp/ripme" target="_blank" rel="noopener noreferrer">RipMe</a> (Java) is an album ripper for various websites.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.httrack.com/" target="_blank" rel="noopener noreferrer">HTTrack Website Copier</a> (Windows/Linux/OSX/Android) can mirror entire websites.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">Other download helpers you should try :</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://board.jdownloader.org/showthread.php?t=54725" target="_blank" rel="noopener noreferrer">Adware free JDownloader</a> (Win/Linux/OSX/Java) Has GUI</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://xdman.sourceforge.net/" target="_blank" rel="noopener noreferrer">xdm</a> (Win/Linux/OSX/Java) Has GUI.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="http://ugetdm.com/" target="_blank" rel="noopener noreferrer">uGet</a> (Win/Linux/OSX/Android) Has GUI.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://curl.haxx.se/" target="_blank" rel="noopener noreferrer">curl</a> (Win/Linux/OSX/...) Command line tool.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://aria2.github.io/" target="_blank" rel="noopener noreferrer">aria2</a> (Linux/OSX) Command line. A web-based UI is also available.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/axel-download-accelerator/axel" target="_blank" rel="noopener noreferrer">axel</a> (Linux/OSX) Command line tool.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://rclone.org/" target="_blank" rel="noopener noreferrer">Rclone</a> (Win/Linux/OSX) Command line tool.<br />Rclone has some great commands that can list files, print remote directory size or even mount it as mountpoint. <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://rclone.org/commands/" target="_blank" rel="noopener noreferrer">Here</a> is a list of all commands. I recommend you to go through their entire website.</p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">You can also use <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/fangfufu/httpdirfs/" target="_blank" rel="noopener noreferrer">httpdirfs</a>, which is made by a redditor who posted it <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/opendirectories/comments/913ojc/i_wrote_a_fuse_filesystem_that_allows_you_to/?utm_content=comments&utm_medium=user&utm_source=reddit&utm_name=frontpage" target="_blank" rel="noopener noreferrer">here</a> to mount the remote directory as mountpoint. It even appears to be somewhat faster than "rclone mount".</p>
<div class="_2UlSUuiYR4BRv_FiLxCcu9">opendirectories-bot</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/u/opendirectories-bot/" target="_blank" rel="noopener noreferrer">/u/opendirectories-bot</a> reports (used to report?) the size along with a list of links to all files in the directory that gets posted in <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/opendirectories/" target="_blank" rel="noopener noreferrer">r/opendirectories</a>. Since it's open source you can download the bot and generate the report yourself. The git-hub repo is available <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/simon987/opendirectories-bot" target="_blank" rel="noopener noreferrer">here</a>, just follow the instructions given in <a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/simon987/opendirectories-bot/blob/master/README.md" target="_blank" rel="noopener noreferrer">README.md</a> file.</p>
<div class="_2UlSUuiYR4BRv_FiLxCcu9">OpenDirectoryDownloader</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://github.com/KoalaBear84/OpenDirectoryDownloader" target="_blank" rel="noopener noreferrer">OpenDirectoryDownloader</a> Indexes open directories listings in 75+ supported formats.</p>
<div class="_1WODZhR-x-fbMu3MOL9cH1">Related Subreddits</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/Archiveteam/" target="_blank" rel="noopener noreferrer">r/Archiveteam</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/DataArchive/" target="_blank" rel="noopener noreferrer">r/DataArchive</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/datacurator/" target="_blank" rel="noopener noreferrer">r/datacurator</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/DataHoarder/" target="_blank" rel="noopener noreferrer">r/DataHoarder</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/JustArchivistThings/" target="_blank" rel="noopener noreferrer">r/JustArchivistThings</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/musichoarder/" target="_blank" rel="noopener noreferrer">r/musichoarder</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/opendirectories/" target="_blank" rel="noopener noreferrer">r/opendirectories</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/rclone/" target="_blank" rel="noopener noreferrer">r/rclone</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://www.reddit.com/r/wget/" target="_blank" rel="noopener noreferrer">r/wget</a></p>
<div class="_1WODZhR-x-fbMu3MOL9cH1">Sites</div>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://the-eye.eu/" target="_blank" rel="noopener noreferrer">The-Eye</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://archive.org/" target="_blank" rel="noopener noreferrer">Archive.org</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM"><a class="_3t5uN8xUmg0TOwRCOGQEcU" href="https://thetrove.net/" target="_blank" rel="noopener noreferrer">The Trove</a></p>
<p class="_1qeIAgB0cPwnLhDF9XSiJM">This is all I could think/know related to open directories. If you think anything else should be included please let me</p>
<p><strong>Interesting URL's</strong></p>
<pre class="a-b-r-La">For Whatsapp(Quick send off)
https://api.whatsapp.com/send?text=insertyourtext
For Telegram(Quick send off)
https://telegram.me/share/url?url=insertyourtext
For Wordpress(Access wp-admin)
https://example.wordpress.com/wp-admin
For Wordpress(Access.opml file,Blogroll links)
https://example.wordpress.com/wp-links-opml.php</pre>
<p><strong>Get Windows 10 Product Key</strong></p>
<ul>
<li>
<blockquote>
<p>powershell "(Get-WmiObject -query ‘select * from SoftwareLicensingService’).OA3xOriginalProductKey"</p>
</blockquote>
</li>
</ul>
<p><strong>MathJax Script for HTML</strong></p>
<p> </p>
<p> </p>
<p><strong>Common Javascript Bookmarks</strong></p>
<p><em>View Cookies</em></p>
<ul>
<li>
<blockquote>
<pre class="a-b-r-La">javascript:alert(document.cookie)</pre>
</blockquote>
</li>
</ul>
<p><em>Youtube Full Screen Bookmark(iOS 11 devices and under,due to webkit)</em></p>
<ul>
<li>
<blockquote>
<p>javascript:document.querySelector(%22video%22).webkitEnterFullScreen%20();</p>
</blockquote>
</li>
</ul>
<p><em>View Outer-HTML Source code for a website</em></p>
<ul>
<li>
<blockquote>
<pre class="a-b-r-La">javascript:(function()%7Bvar%20a=window.open('about:blank').document;a.write('%3C!DOCTYPE%20html%3E%3Chtml%3E%3Chead%3E%3Ctitle%3ESource%20of%20'+location.href+'%3C/title%3E%3Cmeta%20name=%22viewport%22%20content=%22width=device-width%22%20/%3E%3C/head%3E%3Cbody%3E%3C/body%3E%3C/html%3E');a.close();var%20b=a.body.appendChild(a.createElement('pre'));b.style.overflow='auto';b.style.whiteSpace='pre-wrap';b.appendChild(a.createTextNode(document.documentElement.innerHTML))%7D)();</pre>
</blockquote>
</li>
</ul>